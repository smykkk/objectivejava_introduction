package Zadanie26;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class University {
   private HashMap<Long, Student> studentHashMap = new HashMap<>();

   public void addNewStudent(Long indexNumber, String name, String surname){
       studentHashMap.put(indexNumber, new Student(indexNumber, name, surname));
   }

   public boolean containsStudent (Long indexNumber){
       return studentHashMap.containsKey(indexNumber);
   }

   public Student getStudent(Long indexNumber){
       if (containsStudent(indexNumber)){
           return studentHashMap.get(indexNumber);
       }
       else throw new NoSuchStudentException(indexNumber);
   }

   public int studentsCount(){
       return studentHashMap.size();
   }

   public void printAllStudents(){
       for (Student student: studentHashMap.values()) {
           System.out.println(student.format(student));;
       }
   }

}
