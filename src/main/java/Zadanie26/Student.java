package Zadanie26;

public class Student implements IStudentFormat {
    protected long numerIndeksu;
    private String imie;
    private String nazwisko;


    public Student(long numerIndeksu, String imie, String nazwisko) {
        this.numerIndeksu = numerIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public long getNumerIndeksu() {
        return numerIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numerIndeksu=" + numerIndeksu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                '}';
    }

    @Override
    public String format(Student student) {
        return ("NumerIndeksu: " + student.getNumerIndeksu() + " Imie i nazwisko: " +
        student.getImie() + " " + student.getNazwisko());
    }
}
