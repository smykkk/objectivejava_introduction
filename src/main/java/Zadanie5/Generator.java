package Zadanie5;

public class Generator {

    private int currentNumber;
    private int rangeMin;
    private int rangeMax;
    private boolean currentFlag;


    public Generator(int currentNumber, int rangeMin, int rangeMax, boolean currentFlag) {
        this.currentNumber = currentNumber;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.currentFlag = currentFlag;
    }

    public int generateNumber(){
        currentNumber++;
        return currentNumber;
    }
    public int generateInRange(){
        if (currentNumber < rangeMax && currentNumber >= rangeMin){
            currentNumber ++;
            return currentNumber;
        }
        else if (currentNumber == rangeMax){
            System.out.println("Reached rangeMax.");
            return currentNumber;
        }
        else if (currentNumber < rangeMin) {
            System.out.println("Current value out of range. Set to 'rangeMin'.");
            currentNumber = rangeMin;
            return currentNumber;
        }
        else {
            System.out.println("Current value out of range. Set to 'rangeMax'");
            currentNumber = rangeMax;
            return currentNumber;
        }
    }
    public int generateNextEvenNumber(){
        if(currentNumber % 2 == 0){
            currentNumber += 2;
        }
        else {
            currentNumber++;
        }
        return currentNumber;
    }
    public int generateNextMultiple (int multiplicator){
        currentNumber = multiplicator * (currentNumber/multiplicator + 1);
        return currentNumber;
    }
    public boolean generateNextFlag(){
        currentFlag = !currentFlag;
        return currentFlag;
    }
    public char generateNextChar(char currentChar){
        int charInt = (int)currentChar;
        if(charInt < 122){
            charInt++;
        }
        else if (charInt == 122){
            charInt = 97;
        }

        return (char)charInt;
    }

    public void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    public void setRangeMin(int rangeMin) {
        this.rangeMin = rangeMin;
    }

    public void setRangeMax(int rangeMax) {
        this.rangeMax = rangeMax;
    }

    @Override
    public String toString() {
        return "Generator{" +
                "currentNumber=" + currentNumber +
                ", rangeMin=" + rangeMin +
                ", rangeMax=" + rangeMax +
                ", currentFlag=" + currentFlag +
                '}';
    }
}
