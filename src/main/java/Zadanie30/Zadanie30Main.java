package Zadanie30;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Zadanie30Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        LocalDateTime ldt;
        DateTimeFormatter dtf;
        String data = "";

        do{

            System.out.println("Podaj format daty np. yyyy-MM-dd HH:mm: ");
            String format = skan.nextLine();
            try {
                dtf = DateTimeFormatter.ofPattern(format);
            }
            catch (IllegalArgumentException ex){
                System.out.println(ex);
                continue;
            }


            System.out.println("Podaj date: ");
            data = skan.nextLine();
            if (data.equals("quit")){
                break;
            }
            try {
                ldt = LocalDateTime.parse(data, dtf);
                System.out.println(ldt);
            }
            catch (DateTimeParseException dtpe){
                System.out.println("Zly format daty");;
            }
        } while (!data.equals("quit"));
    }
}
