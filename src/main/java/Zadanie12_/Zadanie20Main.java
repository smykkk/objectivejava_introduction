package Zadanie12_;

import java.util.ArrayList;
import java.util.List;

public class Zadanie20Main {
    public static void main(String[] args) {
        List<Figure> figureList = new ArrayList<>();
        Square square1 = new Square(9);
        Circle circle1 = new Circle(8);
        Rectangle rectangle1 = new Rectangle(8, 9);

        figureList.add(new Circle(9));
        figureList.add(new Square(9));
        figureList.add(new Rectangle(5, 4));
        figureList.add(new Rectangle(5, 9));
        figureList.add(new Square(3));
        figureList.add(new Circle(2.8));

        for (Figure fig : figureList) {
            if (fig instanceof Circle){
                System.out.println("Circle: ");
            }
            else if (fig instanceof Square) {
                System.out.println("Square: ");
            }
            else if (fig instanceof Rectangle){
                System.out.println("Rectangle: ");
            }
            System.out.println("Perimeter: " + fig.calculatePerimeter());
            System.out.println("Area: " + fig.calculateArea());
            System.out.println();
        }
    }
}
