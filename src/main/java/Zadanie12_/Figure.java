package Zadanie12_;

public abstract class Figure {
    public abstract double calculatePerimeter();
    public abstract double calculateArea();
}
