package Zadanie12_;

public class Square extends Figure {

    private double a;

    public Square(double a) {
        this.a = a;
    }

    public double calculateArea(){
        return Math.pow(a, 2);
    }
    public double calculatePerimeter(){
        return 4*a;
    }
}
