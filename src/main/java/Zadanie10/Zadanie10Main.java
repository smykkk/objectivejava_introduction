package Zadanie10;

import java.util.Scanner;

public class Zadanie10Main {
    public static void main(String[] args) {
        Car car = new Car();
        System.out.println(car.getCarName());
        Scanner skan = new Scanner(System.in);
        String inputLine = "";

        while (!inputLine.equals("quit")){
            System.out.println("Choose one of the following options: ");
            System.out.println("add/remove (passengers), speedup + kilometres, slowdown + kilometres, getstatus");
            inputLine = skan.nextLine();
            String[] inputArguments = inputLine.split(" ");

            if(inputArguments[0].equals("quit")){
                break;
            }

            else if(inputLine.equals("add")){
                car.addPassenger();
            }
            else if(inputLine.equals("remove")){
                car.removePassenger();
            }
            else if(inputArguments[0].equals("speedup")){
                try {
                    car.speedIncrease(Integer.parseInt(inputArguments[1]));
                }
                catch (NumberFormatException nfe){
                    System.out.println("Inorrect format of acceleration.");
                }
                catch (ArrayIndexOutOfBoundsException aioob){
                    System.out.println("You have not given the speed difference.");
                }
            }
            else if(inputArguments[0].equals("slowdown")){
                try {
                    car.speedDecrease(Integer.parseInt(inputArguments[1]));
                }
                catch (NumberFormatException nfe){
                    System.out.println("Incorrect format of deceleration.");
                }
                catch (ArrayIndexOutOfBoundsException aioob){
                    System.out.println("You have not given the speed difference.");
                }
            }
            else if(inputArguments[0].equals("getstatus")){
                System.out.println(car);
            }
            else {
                System.out.println("Wrong command.");
            }
            System.out.println();
        }
    }
}
