package Zadanie35;

import java.util.Scanner;

public class Zadanie35Main {

    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";

        Department dep1 = new Department();

//        dep1.addOffice("Gdańsk");
//        dep1.addOffice("Sopot");
//        dep1.addOffice("Gdynia");



        do {
            System.out.println("Enter 'handle' + PESEL, case type (register/unregister/check_status, office city.");
            System.out.println("Alternatively enter 'add (office)' + city or 'get (office) + city.");
            inputLine = skan.nextLine();
            if(inputLine.equals("quit")){
                break;
            }
            else {
                try {
                    String[] inputArguments = inputLine.split(" ");
                    if(inputArguments[0].equals("handle")){
                        int pesel = Integer.parseInt(inputArguments[1]);
                        typSprawy typ = typSprawy.valueOf(inputArguments[2].toUpperCase());
                        String biuro = inputArguments[3];
                        dep1.getOffice(biuro).addClient(pesel, typ);
                        dep1.getOffice(biuro).handle(dep1.getOffice(biuro).getClient(pesel));
                    }
                    else if(inputArguments[0].equals("add")){
                        String city = inputArguments[1];
                        dep1.addOffice(city);
                    }
                    else if(inputArguments[0].equals("get")){
                        String city = inputArguments[1];
                        System.out.println("Office in " + city + " is currently handling " +
                                dep1.getOffice(city).getMapaKlientow().size() + " clients.");
                    }
                    else{
                        System.out.println("Incorrect command.");
                    }

                }catch (NumberFormatException nfe){
                    System.out.println("Incorrect pesel format.");
                }catch (ArrayIndexOutOfBoundsException aioobe){
                    System.out.println("Not enough arguments.");
                }
            }
            System.out.println();
            System.out.println();

        }while (!inputLine.equals("quit"));
    }
}
