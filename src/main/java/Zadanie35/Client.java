package Zadanie35;

public class Client {
    private typSprawy typ;
    private int PESEL;

    public Client(typSprawy typ, int PESEL) {
        this.typ = typ;
        this.PESEL = PESEL;
    }

    public typSprawy getTyp() {
        return typ;
    }

    public void setTyp(typSprawy typ) {
        this.typ = typ;
    }

    public int getPESEL() {
        return PESEL;
    }

    public void setPESEL(int PESEL) {
        this.PESEL = PESEL;
    }
}
