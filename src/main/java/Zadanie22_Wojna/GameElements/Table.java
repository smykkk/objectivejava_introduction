package Zadanie22_Wojna.GameElements;

import java.util.ArrayList;

public class Table {
    public ArrayList<Card> player1Stack = new ArrayList<>();
    public ArrayList<Card> player2Stack = new ArrayList<>();
    public ArrayList<Card> tableCards = new ArrayList<>();

    //add all cards from stacks to one list
    public void consolidateCards(){
        tableCards.clear();
        tableCards.addAll(player2Stack);
        tableCards.addAll(player1Stack);
        player1Stack.clear();
        player2Stack.clear();
    }

}
