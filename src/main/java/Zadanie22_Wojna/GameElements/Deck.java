package Zadanie22_Wojna.GameElements;

import java.util.ArrayList;

public class Deck {
    private String color;
    // deck jest kontenerem. lista powinna byc prywatna i dla kazdej operacji decka (dodawanie usuwanie kart itd.) powinna istniec metoda.
    // np. metoda addCard, removeCard, getFirstCard, shuffleDeck itd.
    public ArrayList<Card> cardList = fillDeck();

    public ArrayList<Card> fillDeck() {
        ArrayList<Card> tempCardList = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            for (Suit suit : Suit.values()) {
                tempCardList.add(new Card(i, suit));
            }
        }
        return tempCardList;

    }

    public ArrayList<Card> getCardList() {
        return cardList;
    }
}
