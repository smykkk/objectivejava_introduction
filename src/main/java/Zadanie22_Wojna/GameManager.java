package Zadanie22_Wojna;

import Zadanie22_Wojna.GameElements.Player;

import java.util.TimerTask;

public class GameManager {

    private Game game;
    private GameStats gameStats = new GameStats();
    // nie powinien sie tutaj znajdowac. przenies tego timera do klasy w ktorej go uzywasz. to samo tyczy sie deklaracji klasy. jesli jest klasa wewnetrzna to powinna sie znalezc w klasie/pliku gdzie jest uzywana
    protected PrintTask printStats = new PrintTask();

    public GameManager(String player1Name, String player2Name) {
        this.game = new Game(new Player(player1Name), new Player(player2Name));
    }

    public void runGame(int gamesToPlay) {
        boolean infinite = isInfinite(gamesToPlay);

        while (playing(infinite, gamesToPlay)) {
            game.play();
            gameStats.updateStats(game.getResults());
            game.getResults().resetForNewGame();
            game.playersSwitchPlaces();
            gamesToPlay--;
        }
        System.out.println("\nFinal statistics:___________________\n");
        gameStats.printStats();
        printSeriesWinner();
    }

    private boolean isInfinite(int gamesToPlay) {
        boolean infinite;
        if (gamesToPlay < 0) infinite = true;
        else infinite = false;
        return infinite;
    }

    private boolean playing(boolean infinite, Integer gamesToPlay) {
        if (infinite) return true;
        else return (gamesToPlay > 0);
    }

    private void printLeader(Player player1, Player player2) {
        if (player1.gamesWon > player2.gamesWon)
            System.out.println(" Player " + player1.getName() + " is winning by: " + (player1.gamesWon - player2.gamesWon) + " wins.");
        else if (player1.gamesWon < player2.gamesWon)
            System.out.println(" Player " + player2.getName() + " is winning by: " + (player2.gamesWon - player1.gamesWon) + " wins.");
        else {
            System.out.println("Scores equal (" + player1.gamesWon + "): winner is yet undecided.");
        }
    }

    private void printSeriesWinner() {
        System.out.println();
        if (game.getPlayer1().gamesWon > game.getPlayer2().gamesWon)
            System.out.println("Player " + game.getPlayer1().getName() + " won the series by " + (game.getPlayer1().gamesWon - game.getPlayer2().gamesWon) + " wins.");
        else if (game.getPlayer1().gamesWon < game.getPlayer2().gamesWon)
            System.out.println("Player " + game.getPlayer2().getName() + " won the series by " + (game.getPlayer2().gamesWon - game.getPlayer1().gamesWon) + " wins.");
        else {
            System.out.println("Scores equal (" + game.getPlayer1().gamesWon + "): winner undecided.");
        }
    }

    private void printPlayersCards(Player player1, Player player2) {
        //wypisz karty graczy
        System.out.println(player1.playerCardList);
        System.out.println(player2.playerCardList);
    }

    private void printResults(GameResults results) {
        System.out.println("Player " + results.winner.getName() + " won in " + results.stageCount + " turns.");
        System.out.println(results.warCount + " wars took place.");
        System.out.println("Game duration millis: " + results.duration);
        System.out.println("Player1: " + game.getPlayer1().playerCardList.size());
        System.out.println("Player2: " + game.getPlayer2().playerCardList.size());
        System.out.println();
    }

    private void printStageResults(Player player1, Player player2, GameResults results) {
        System.out.println();
        System.out.println("<<<<<<<<<<<<<<<<<<<<");
        System.out.println("_________________" + player1.currentCardValue + " vs " + player2.currentCardValue);
        System.out.println("StageCount: " + results.stageCount);
        System.out.println("WarCount: " + results.warCount);
        System.out.println(player1.playerCardList.size() + " vs " + player2.playerCardList.size() + "cards");

    }

    public class PrintTask extends TimerTask {

        long lastCount = 0;

        // todo add average speed
        long averageSpeed = 0;

        @Override
        public void run() {

//            printResults(game.getResults());
            gameStats.printStats();
            printLeader(game.getPlayer1(), game.getPlayer2());
            long newCount = gameStats.getTotalGameCount();
            long speed = (newCount - lastCount) / 2;
            System.out.printf("%24s %d \n", "Games per second:", speed);
            lastCount = newCount;

        }
    }


}
