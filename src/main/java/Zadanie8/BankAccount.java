package Zadanie8;

public class BankAccount {
    private int amount = 0;

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int addMoney(int quote){
        amount += quote;
        return amount;
    }

    public int subtractMoney(int quote){
        amount -= quote;
        return amount;
    }

    public void printAccountStatus(){
        System.out.println(amount);
    }
}
