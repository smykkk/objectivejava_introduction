package Zadanie8;

import java.util.Scanner;

public class Zadanie8Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine;

        System.out.println("Enter initial money amount:");
        BankAccount bankAccount = new BankAccount();
        try {
            inputLine = skan.nextLine();
            bankAccount.setAmount(Integer.parseInt(inputLine));
        }
        catch (NumberFormatException nfe){
            System.out.println("Incorrect number format. Set default to 0.");
        }


        do {
            System.out.println("Enter action [add/sub/status] and quote if needed:");
            inputLine = skan.nextLine();

            try {
                if(inputLine.split(" ")[0].equals("add")){
                    bankAccount.addMoney(Integer.parseInt(inputLine.split(" ", 2)[1]));

                }
                if(inputLine.split(" ")[0].equals("sub")){
                    bankAccount.subtractMoney(Integer.parseInt(inputLine.split(" ", 2)[1]));
                }
                if(inputLine.equals("status")){
                    bankAccount.printAccountStatus();
                }
            }
            catch (NumberFormatException nfe){
                System.out.println("Incorrect argument(s).");
            }
            catch (ArrayIndexOutOfBoundsException ioobe){
                System.out.println("No quote given.");
            }


        } while (!inputLine.equals("quit"));
    }
}

//sprawdzic error handling i prompty