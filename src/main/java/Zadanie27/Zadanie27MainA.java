package Zadanie27;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie27MainA {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<Integer>();
        System.out.println("Dodaj 5 kolejnych liczb a ja je wypisze w liscie");
        Scanner skan = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {
            System.out.println("liczba: ");
            lista.add(skan.nextInt());
        }

        System.out.println("Lista podanych elementow");

        for (int element : lista) {
            System.out.print(element + " ");
        }
    }
}
