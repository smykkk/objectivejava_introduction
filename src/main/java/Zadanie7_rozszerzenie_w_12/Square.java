package Zadanie7_rozszerzenie_w_12;

public class Square {
    private double squareLeg;



    public void setSquareLeg(double squareLeg) {
        this.squareLeg = squareLeg;
    }

    public double calculatePerimeter(){
        return squareLeg * 4;
    }
    public double calculateArea(){
        return Math.pow(squareLeg, 2);
    }
}
