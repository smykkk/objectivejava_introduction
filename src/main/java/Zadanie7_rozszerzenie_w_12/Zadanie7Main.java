package Zadanie7_rozszerzenie_w_12;

import java.util.Scanner;

public class Zadanie7Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj bok kwadratu:");

        Square square = new Square();
        square.setSquareLeg(skan.nextDouble());
        System.out.println("Pole to: " + square.calculateArea());
        System.out.println("Obwod kwadratu to: " + square.calculatePerimeter());

        Rectangle rectangle = new Rectangle();
        System.out.println("Podaj dwie dlugosci bokow prostokata: ");
        String linia = skan.nextLine();
        rectangle.setLegA(Double.parseDouble(linia.split("", 2)[0]));
        rectangle.setLegB(Double.parseDouble(linia.split("", 2)[1]));
        System.out.println("Pole prostokata to: " + rectangle.calculateArea());
        System.out.println("Obwod prostokata to: " + rectangle.calculatePerimeter());
    }
}