package Zadanie21;

public class TownsMan extends Citizen {

    public TownsMan(String name) {
        super(name);
    }

    @Override
    protected boolean canVote() {
        return true;
    }
}
