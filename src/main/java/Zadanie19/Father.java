package Zadanie19;

public class Father extends FamilyMember {
    public Father(String name) {
        super(name);
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
