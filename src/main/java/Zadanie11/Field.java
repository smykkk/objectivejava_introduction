package Zadanie11;

public class Field {
    private boolean[][] table = new boolean[10][10];

    public void newGame(){
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9 ; j++) {
                table[i][j] = false;
            }
        }
    }

    public void printField(){
        System.out.println("  0 1 2 3 4 5 6 7 8 9 (x)");
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < 10; j++) {
                if(table[i][j]){
                    System.out.print("X ");
                }
                else {
                    System.out.print("O ");
                }
            }
            System.out.println();
        }
        System.out.println("(y)");
    }

    public void checkCell(int x, int y){
        if(!table[y][x]){
            table[y][x] = true;
        }
        else {
            System.out.println("You've already checked this one.");
        }
    }


}
