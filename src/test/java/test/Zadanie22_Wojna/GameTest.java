package test.Zadanie22_Wojna;

import Zadanie22_Wojna.Game;
import Zadanie22_Wojna.GameElements.Player;
import org.hamcrest.core.CombinableMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class GameTest {

    private Player player1 = new Player("Jan");
    private Player player2 = new Player("Tomasz");
    private Game game = new Game(player1, player2);

    @Before
    public void resetGame(){
        player1 = new Player("Jan");
        player2 = new Player("Tomasz");
        game = new Game(player1, player2);
    }

    @Test
    public void playersSwitchPlaces() {
        game.playersSwitchPlaces();
        Assert.assertEquals("Tomasz", game.getPlayer1().getName());
    }

    @Test
    public void deckAfterGameContains52Cards() {
        game.play();
        Assert.assertEquals(52, game.getDeck().getCardList().size());
    }

    @Test
    public void NumberOfWinsIncrementsAfterGame(){
        game.play();
        Assert.assertThat(1, either(equalTo(player1.gamesWon)).or(equalTo(player2.gamesWon)));
    }
}
